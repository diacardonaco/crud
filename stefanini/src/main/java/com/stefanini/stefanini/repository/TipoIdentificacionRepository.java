package com.stefanini.stefanini.repository;

import com.stefanini.stefanini.entity.Persona;
import com.stefanini.stefanini.entity.TipoIdentificacion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TipoIdentificacionRepository extends JpaRepository<TipoIdentificacion, Integer> {

    Optional<TipoIdentificacion> findByNombre(String nombre);
    boolean existsByNombre(String nombre);

}