package com.stefanini.stefanini.service;

import com.stefanini.stefanini.entity.Estado;
import com.stefanini.stefanini.entity.TipoIdentificacion;
import com.stefanini.stefanini.repository.EstadoRepository;
import com.stefanini.stefanini.repository.TipoIdentificacionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TipoIdentificacionService {
    @Autowired
    TipoIdentificacionRepository tipoIdentificacionRepository;

    public List<TipoIdentificacion> list(){
        return  tipoIdentificacionRepository.findAll();
    }


    public Optional<TipoIdentificacion> getOne(int id){
        return  tipoIdentificacionRepository.findById(id);
    }

    public Optional<TipoIdentificacion> getByNombre(String nombre){
        return  tipoIdentificacionRepository.findByNombre(nombre);
    }

    public void save(TipoIdentificacion tipoIdentificacion){
        tipoIdentificacionRepository.save(tipoIdentificacion);
    }

    public void delete(int id){
        tipoIdentificacionRepository.deleteById(id);
    }

    public boolean existsById(int id){
        return tipoIdentificacionRepository.existsById(id);
    }

    public boolean existsByNombre(String nombre){
        return tipoIdentificacionRepository.existsByNombre(nombre);
    }



}
