package com.stefanini.stefanini.service;

import com.stefanini.stefanini.entity.Estado;
import com.stefanini.stefanini.repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class EstadoService {

    @Autowired
    EstadoRepository estadoRepository;

    public List<Estado> list(){
        return  estadoRepository.findAll();
    }


    public Optional<Estado> getOne(int id){
        return  estadoRepository.findById(id);
    }

    public Optional<Estado> getByNombre(String nombre){
        return  estadoRepository.findByNombre(nombre);
    }

    public void save(Estado persona){
        estadoRepository.save(persona);
    }

    public void delete(int id){
        estadoRepository.deleteById(id);
    }

    public boolean existsById(int id){
        return estadoRepository.existsById(id);
    }

    public boolean existsByNombre(String nombre){
        return estadoRepository.existsByNombre(nombre);
    }
}
