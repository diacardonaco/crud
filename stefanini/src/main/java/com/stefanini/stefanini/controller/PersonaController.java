package com.stefanini.stefanini.controller;

import com.stefanini.stefanini.entity.Persona;
import com.stefanini.stefanini.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/persona")
public class PersonaController {

    @Autowired
    PersonaService personaService;

    @GetMapping("/personas")
    public ResponseEntity<List<Persona>> list(){
        List<Persona> list  = personaService.list();
        return  new ResponseEntity<List<Persona>>(list, HttpStatus.OK);
    }

    @GetMapping("/personaporcodigo/{codigo}")
    public ResponseEntity<Persona> getById(@PathVariable("codigo") int codigo){
        List<Persona> list  = personaService.list();
        if(!personaService.existsById(codigo)){
            return  new ResponseEntity("no existe", HttpStatus.OK);
        }
        Optional<Persona> persona = personaService.getOne(codigo);
        return  new ResponseEntity(persona, HttpStatus.OK);
    }


    @PostMapping(value = "/guardarPersona")
    public ResponseEntity<Persona> guardarPersona(@RequestBody Persona persona){
        personaService.save(persona);
        return  new ResponseEntity(persona, HttpStatus.OK);
    }


    @DeleteMapping(value = "/eliminarPersona/{codigo}")
    public ResponseEntity<Persona> eliminarPersona(@PathVariable("codigo") int codigo){
        personaService.delete(codigo);
        return  new ResponseEntity("usuario eliminado", HttpStatus.OK);
    }


    @PutMapping(value = "/actualizarPersona")
    public ResponseEntity<Persona> actualizarPersona(@RequestBody Persona persona){
        personaService.save(persona);
        return  new ResponseEntity(persona, HttpStatus.OK);
    }





}
