package com.stefanini.stefanini.controller;

import com.stefanini.stefanini.entity.Estado;
import com.stefanini.stefanini.entity.Persona;
import com.stefanini.stefanini.entity.TipoIdentificacion;
import com.stefanini.stefanini.service.TipoIdentificacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/tipoIdentificacion")
public class TipoIdentificacionController {


    @Autowired
    TipoIdentificacionService tipoIdentificacionService;

    @GetMapping("/identificaciones")
    public ResponseEntity<List<TipoIdentificacion>> list(){
        List<TipoIdentificacion> list  = tipoIdentificacionService.list();
        return  new ResponseEntity<List<TipoIdentificacion>>(list, HttpStatus.OK);
    }

    @GetMapping("/tipoidentificacionporcodigo/{codigo}")
    public ResponseEntity<Persona> getById(@PathVariable("codigo") int codigo){
        List<TipoIdentificacion> list  = tipoIdentificacionService.list();
        if(!tipoIdentificacionService.existsById(codigo)){
            return  new ResponseEntity("no existe", HttpStatus.OK);
        }
        Optional<TipoIdentificacion> tipoIdentificacion = tipoIdentificacionService.getOne(codigo);
        return  new ResponseEntity(tipoIdentificacion, HttpStatus.OK);
    }


    @PostMapping(value = "/guardarTipoIdentificacion")
    public ResponseEntity<TipoIdentificacion> guardarEstado(@RequestBody TipoIdentificacion tipoIdentificacion){
        tipoIdentificacionService.save(tipoIdentificacion);
        return  new ResponseEntity(tipoIdentificacion, HttpStatus.OK);
    }


    @DeleteMapping(value = "/eliminarTipoIdentificacion/{codigo}")
    public ResponseEntity<TipoIdentificacion> eliminarTipoIdentificacion(@PathVariable("codigo") int codigo){
        tipoIdentificacionService.delete(codigo);
        return  new ResponseEntity("tipo identificacion eliminado", HttpStatus.OK);
    }


    @PutMapping(value = "/actualizarTipoIdentificacion")
    public ResponseEntity<TipoIdentificacion> actualizarTipoIdentificacion(@RequestBody TipoIdentificacion tipoIdentificacion){
        tipoIdentificacionService.save(tipoIdentificacion);
        return  new ResponseEntity(tipoIdentificacion, HttpStatus.OK);
    }










}
